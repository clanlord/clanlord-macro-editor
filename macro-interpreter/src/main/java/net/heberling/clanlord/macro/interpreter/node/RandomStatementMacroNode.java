package net.heberling.clanlord.macro.interpreter.node;

public class RandomStatementMacroNode extends AbstractSequenceMacroNode {
  private static final java.util.Random random = new java.util.Random();

  private int lastChosen = -1;

  private int currentChosen = -1;

  private boolean noRepeat;

  public RandomStatementMacroNode(boolean noRepeat) {
    super("random");
    this.noRepeat = noRepeat;
  }

  public Result tick() {
    if (statements.size() > 1) {
      // more than one statement
      if (currentChosen == -1) {
        // choose a new one
        do {
          currentChosen = random.nextInt(statements.size());
        } while (noRepeat && currentChosen == lastChosen);
        lastChosen = currentChosen;
      }
      Result result = statements.get(currentChosen).tick();
      if (result.isFinished()) {
        // if the statement is finished, we need to choose a new one next time,
        // otherwise we need to make sure to go back into the same statement next time
        currentChosen = -1;
      }
      return result;
    } else if (statements.size() == 1) {
      // one statement, we need to use it
      return statements.get(0).tick();
    } else {
      // no statements, we are finished
      return new Result(true, 0, null, null);
    }
  }

  @Override
  public AbstractMacroNode copy(AbstractMacroNode newParent) {
    RandomStatementMacroNode randomStatementMacroNode = new RandomStatementMacroNode(noRepeat);
    randomStatementMacroNode.setParent(newParent);
    // no-repeat needs to work even after the macro is restarted
    randomStatementMacroNode.currentChosen = currentChosen;
    statements.forEach(
        s -> randomStatementMacroNode.addChildMacroNode(s.copy(randomStatementMacroNode)));
    return randomStatementMacroNode;
  }
}
