package net.heberling.clanlord.macro.interpreter.node;

import java.util.function.Consumer;

public abstract class AbstractTerminalMacroNode extends AbstractMacroNode {
  @Override
  public void visit(Consumer<AbstractMacroNode> consumer) {
    consumer.accept(this);
  }
}
