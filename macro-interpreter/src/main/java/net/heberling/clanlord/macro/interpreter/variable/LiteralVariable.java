package net.heberling.clanlord.macro.interpreter.variable;

/** Contains a number or a string. */
public class LiteralVariable extends Variable {
  private final String content;

  private Integer number;

  public LiteralVariable(int content) {
    this.content = String.valueOf(content);
    number = content;
  }

  public LiteralVariable(String content) {
    this.content = content;
    try {
      this.number = Integer.parseInt(content);
    } catch (NumberFormatException e) {
      // TODO: do a check before parsing and don't handle this with an exception
      this.number = null;
    }
  }

  @Override
  public String asString() {
    return content;
  }

  @Override
  public int asNumber() {
    if (!isNumeric()) {
      throw new IllegalStateException(content + " is not numeric");
    }
    return number;
  }

  @Override
  public boolean isNumeric() {
    return number != null;
  }
}
