package net.heberling.clanlord.macro.interpreter.node;

import net.heberling.clanlord.macro.interpreter.variable.Variable;
import net.heberling.clanlord.macro.parser.MacroParser;

public class PauseMacroNode extends AbstractTerminalMacroNode {

  private final MacroParser.ValueContext pauseLength;

  public PauseMacroNode(MacroParser.ValueContext pauseLength) {
    this.pauseLength = pauseLength;
  }

  public Result tick() {
    Variable variable;
    int pauseLenght;
    if (this.pauseLength.NUMBER() != null) {
      pauseLenght = Integer.parseInt(this.pauseLength.NUMBER().getText());
    } else if (this.pauseLength.IDENTIFIER() != null
        && (variable = getVariableStorage().get(this.pauseLength.getText())) != null) {
      pauseLenght = Integer.parseInt(variable.asString());
    } else {
      throw new IllegalArgumentException("pause must have a numeric papameter");
    }
    return new Result(true, pauseLenght, null, null);
  }

  @Override
  public AbstractMacroNode copy(AbstractMacroNode newParent) {
    PauseMacroNode pauseMacroNode = new PauseMacroNode(pauseLength);
    pauseMacroNode.setParent(newParent);
    return pauseMacroNode;
  }

  @Override
  public String toString() {
    return "pause " + pauseLength;
  }
}
