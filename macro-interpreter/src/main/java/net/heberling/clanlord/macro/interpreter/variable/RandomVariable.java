package net.heberling.clanlord.macro.interpreter.variable;

/** Returns a random value every time it is read */
public class RandomVariable extends Variable {
  private static final java.util.Random random = new java.util.Random();

  @Override
  public String asString() {
    return String.valueOf(asNumber());
  }

  @Override
  public int asNumber() {
    return random.nextInt(10000);
  }

  @Override
  public boolean isNumeric() {
    return true;
  }
}
