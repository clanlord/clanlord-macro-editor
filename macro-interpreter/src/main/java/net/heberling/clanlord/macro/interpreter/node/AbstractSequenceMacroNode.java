package net.heberling.clanlord.macro.interpreter.node;

import java.util.LinkedList;
import java.util.function.Consumer;

public abstract class AbstractSequenceMacroNode extends AbstractMacroNode {
  protected final LinkedList<AbstractMacroNode> statements;
  private final String name;

  public AbstractSequenceMacroNode(String name) {
    this.name = name;
    statements = new LinkedList<>();
  }

  public void addChildMacroNode(AbstractMacroNode abstractMacroNode) {
    if (abstractMacroNode != null) {
      statements.addLast(abstractMacroNode);
    }
  }

  @Override
  public void visit(Consumer<AbstractMacroNode> consumer) {
    consumer.accept(this);
    statements.forEach(node -> node.visit(consumer));
  }

  public String getName() {
    return name;
  }

  @Override
  public String toString() {
    return name;
  }
}
