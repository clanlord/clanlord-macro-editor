package net.heberling.clanlord.macro.interpreter.variable;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/** @author markus */
public class VariableStorage {

  private static final Pattern NUM_LETTERS = Pattern.compile("(.*)\\.num_letters");
  private static final Pattern LETTER = Pattern.compile("(.*)\\.letter\\[(.*)]");
  private static final Pattern NUM_WORDS = Pattern.compile("(.*)\\.num_words");
  private static final Pattern WORD = Pattern.compile("(.*)\\.word\\[(.*)]");

  private static final VariableStorage GLOBAL_STORAGE = new VariableStorage();

  private final HashMap<String, Variable> localVars = new HashMap<>();

  public Variable get(String key) {
    String action = "None";
    int index = 0;
    Matcher matcher;
    if ((matcher = NUM_LETTERS.matcher(key)).matches()) {
      key = matcher.group(1);
      action = "num_letters";
    } else if ((matcher = LETTER.matcher(key)).matches()) {
      key = matcher.group(1);
      index = Integer.parseInt(matcher.group(2));
      action = "letter";
    } else if ((matcher = NUM_WORDS.matcher(key)).matches()) {
      key = matcher.group(1);
      action = "num_words";
    } else if ((matcher = WORD.matcher(key)).matches()) {
      key = matcher.group(1);
      index = Integer.parseInt(matcher.group(2));
      action = "word";
    }

    Variable variable = _get(key);

    switch (action) {
      case "num_letters":
        variable = variable.getLetterCount();
        break;
      case "letter":
        variable = variable.getLetter(index);
        break;
      case "num_words":
        variable = variable.getWordCount();
        break;
      case "word":
        variable = variable.getWord(index);
        break;
      case "None":
        // do nothing
    }

    return variable;
  }

  private Variable _get(String key) {
    String keyLower = key.toLowerCase();
    Variable variable = null;
    if (localVars.containsKey(keyLower)) variable = localVars.get(keyLower);
    else if (this != GLOBAL_STORAGE) variable = GLOBAL_STORAGE.get(key);

    // Variable not found, we create a variable with its name as content, but don't store it
    if (variable == null) {
      variable = new LiteralVariable(key);
    }

    if (!variable.isNumeric()
        && variable.getWordCount().asNumber() == 1
        && variable.getLetterCount().asNumber() > 0
        && !keyLower.equals(variable.asString().toLowerCase())) {
      // single word variable, could be a reference to another variable, so we need to evaluate it
      variable = _get(variable.asString());
    }

    return variable;
  }

  public void put(String key, Variable value) {
    key = key.toLowerCase();
    localVars.put(key, value);
  }

  public static VariableStorage getGlobalStorage() {
    return GLOBAL_STORAGE;
  }

  public void clear() {
    localVars.clear();
  }
}
