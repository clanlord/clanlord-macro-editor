package net.heberling.clanlord.macro.interpreter;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.SequenceInputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.stream.Collectors;
import net.heberling.clanlord.macro.interpreter.node.AbstractMacroNode;
import net.heberling.clanlord.macro.interpreter.node.CallMacroNode;
import net.heberling.clanlord.macro.interpreter.node.GotoMacroNode;
import net.heberling.clanlord.macro.interpreter.node.IfMacroNode;
import net.heberling.clanlord.macro.interpreter.node.LabelMacroNode;
import net.heberling.clanlord.macro.interpreter.node.MacroMacroNode;
import net.heberling.clanlord.macro.interpreter.node.MessageStatementMacroNode;
import net.heberling.clanlord.macro.interpreter.node.PauseMacroNode;
import net.heberling.clanlord.macro.interpreter.node.RandomStatementMacroNode;
import net.heberling.clanlord.macro.interpreter.node.SequenceMacroNode;
import net.heberling.clanlord.macro.interpreter.node.SetMacroNode;
import net.heberling.clanlord.macro.interpreter.node.TextCommandStatementMacroNode;
import net.heberling.clanlord.macro.parser.MacroBaseListener;
import net.heberling.clanlord.macro.parser.MacroLexer;
import net.heberling.clanlord.macro.parser.MacroParser;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.RuleContext;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.tree.ParseTreeProperty;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MacroCompiler extends MacroBaseListener {
  private static final Logger LOGGER = LoggerFactory.getLogger(MacroCompiler.class);
  private final ParseTreeProperty<AbstractMacroNode> treeMap = new ParseTreeProperty<>();
  private final HashMap<String, MacroMacroNode> macroMap = new HashMap<>();
  private final MacroCallback callback;

  public MacroCompiler(MacroCallback callback) {
    this.callback = callback;
  }

  @Override
  public void enterInclude_macro(MacroParser.Include_macroContext ctx) {
    try {
      // try to parse with our parser
      MacroLexer macroLexer =
          new MacroLexer(
              CharStreams.fromStream(
                  // The parser expects a newline before EOF, so make sure there is one
                  new SequenceInputStream(
                      new FileInputStream(
                          callback.getMacroFile(
                              ctx.QUOTED()
                                  .getText()
                                  .substring(1, ctx.QUOTED().getText().length() - 1))),
                      new ByteArrayInputStream(new byte[] {10}))));
      macroLexer.addErrorListener(new BailErrorListener());

      MacroParser macroParser = new MacroParser(new CommonTokenStream(macroLexer));
      macroParser.addErrorListener(new BailErrorListener());

      ParseTreeWalker.DEFAULT.walk(this, macroParser.macros());
    } catch (IOException e) {
      throw new BailErrorListener.BailException(
          e.getMessage(),
          ctx.getStart().getLine(),
          ctx.getStart().getCharPositionInLine(),
          ctx.getStart().getStartIndex());
    }
  }

  @Override
  public void enterSet_macro(MacroParser.Set_macroContext ctx) {
    final SetMacroNode setMacroNode =
        new SetMacroNode(ctx.IDENTIFIER().getText(), true, ctx.set_expression());
    setMacroNode.setParent(new MacroMacroNode("dummy"));
    setMacroNode.tick();
  }

  @Override
  public void enterIdentifier_macro(MacroParser.Identifier_macroContext ctx) {
    MacroMacroNode macroMacroNode = new MacroMacroNode(ctx.IDENTIFIER().getText().trim());
    addTreeNode(ctx, macroMacroNode);

    LOGGER.debug("Registering macro trigger: " + ctx.IDENTIFIER().getText());

    // TODO: this allows recursion, do we really need that?
    macroMap.put(ctx.IDENTIFIER().getText(), macroMacroNode);
  }

  @Override
  public void exitIdentifier_macro(MacroParser.Identifier_macroContext ctx) {
    MacroMacroNode macroMacroNode = (MacroMacroNode) getMacroNode(ctx);
    macroMacroNode.setStatement(getMacroNode(ctx.statement()));
  }

  @Override
  public void enterExpression_macro(MacroParser.Expression_macroContext ctx) {
    MacroMacroNode macroMacroNode = new MacroMacroNode(ctx.trigger.getText().trim());
    addTreeNode(ctx, macroMacroNode);
  }

  @Override
  public void exitExpression_macro(MacroParser.Expression_macroContext ctx) {
    MacroMacroNode macroMacroNode = (MacroMacroNode) getMacroNode(ctx);
    macroMacroNode.setStatement(getMacroNode(ctx.statement()));

    String trigger = ctx.trigger.getText();
    if (macroMacroNode.getAttributes().contains(MacroMacroNode.Attribute.IGNORE_CASE)) {
      trigger = trigger.toLowerCase();
    }
    LOGGER.debug("Registering macro trigger: " + trigger);
    macroMap.put(trigger, macroMacroNode);
  }

  @Override
  public void enterReplacement_macro(MacroParser.Replacement_macroContext ctx) {
    MacroMacroNode macroMacroNode = new MacroMacroNode(ctx.SINGLE_QUOTED().getText().trim());
    addTreeNode(ctx, macroMacroNode);
  }

  @Override
  public void exitReplacement_macro(MacroParser.Replacement_macroContext ctx) {
    MacroMacroNode macroMacroNode = (MacroMacroNode) getMacroNode(ctx);

    macroMacroNode.setStatement(getMacroNode(ctx.statement()));

    String trigger = ctx.SINGLE_QUOTED().getText();
    if (macroMacroNode.getAttributes().contains(MacroMacroNode.Attribute.IGNORE_CASE)) {
      trigger = trigger.toLowerCase();
    }
    LOGGER.debug("Registering macro trigger: " + trigger);
    macroMap.put(trigger, macroMacroNode);
  }

  @Override
  public void enterKey_macro(MacroParser.Key_macroContext ctx) {
    int keycode = getKeyCodeFromString(ctx.key.getText());
    int modifiers =
        getModifiersFromString(
            ctx.modifier.stream().map(Token::getText).collect(Collectors.joining("-")));
    String macroTrigger = modifiers + "-" + keycode;

    MacroMacroNode macroMacroNode = new MacroMacroNode(macroTrigger);
    addTreeNode(ctx, macroMacroNode);

    LOGGER.debug("Registering macro trigger: " + macroTrigger);
    macroMap.put(macroTrigger, macroMacroNode);
  }

  @Override
  public void exitKey_macro(MacroParser.Key_macroContext ctx) {
    MacroMacroNode macroMacroNode = (MacroMacroNode) getMacroNode(ctx);

    macroMacroNode.setStatement(getMacroNode(ctx.statement()));
  }
  /*
   * private String getMacroTrigger(MacroParser.TriggerContext trigger) { if (trigger.SINGLE_QUOTED() != null) {
   * return trigger.SINGLE_QUOTED().getText(); } else if (trigger.QUOTED() != null) { return
   * trigger.QUOTED().getText(); } else { int keycode = getKeyCodeFromString(trigger.IDENTIFIER().getText()); int
   * modifiers = getModifiersFromString(trigger.IDENTIFIER().getText());
   *
   * return modifiers + "-" + keycode; } }
   */

  public static int getKeyCodeFromString(String s) {
    // check for numpad
    boolean numpad = s.contains("numpad");

    // remove modifiers
    if (s.contains("-")) s = s.substring(s.lastIndexOf("-") + 1);

    s = s.trim().toLowerCase();

    if (s.equals("f1")) return KeyEvent.VK_F1;
    else if (s.equals("f2")) return KeyEvent.VK_F2;
    else if (s.equals("f3")) return KeyEvent.VK_F3;
    else if (s.equals("f4")) return KeyEvent.VK_F4;
    else if (s.equals("f5")) return KeyEvent.VK_F5;
    else if (s.equals("f6")) return KeyEvent.VK_F6;
    else if (s.equals("f7")) return KeyEvent.VK_F7;
    else if (s.equals("f8")) return KeyEvent.VK_F8;
    else if (s.equals("f9")) return KeyEvent.VK_F9;
    else if (s.equals("f10")) return KeyEvent.VK_F10;
    else if (s.equals("f11")) return KeyEvent.VK_F11;
    else if (s.equals("f12")) return KeyEvent.VK_F12;
    else if (s.equals("f13")) return KeyEvent.VK_F13;
    else if (s.equals("f14")) return KeyEvent.VK_F14;
    else if (s.equals("f15")) return KeyEvent.VK_F15;
    else if (s.equals("escape")) return KeyEvent.VK_ESCAPE;
    else if (s.equals("minus")) return KeyEvent.VK_MINUS;
    else if (s.equals("delete")) return KeyEvent.VK_DELETE;
    else if (s.equals("tab")) return KeyEvent.VK_TAB;
    else if (s.equals("return")) return KeyEvent.VK_ENTER;
    else if (s.equals("space")) return KeyEvent.VK_SPACE;
    else if (s.equals("help")) return KeyEvent.VK_HELP;
    else if (s.equals("home")) return KeyEvent.VK_HOME;
    else if (s.equals("undo")) return KeyEvent.VK_UNDO;
    else if (s.equals("del")) return KeyEvent.VK_DELETE;
    else if (s.equals("end")) return KeyEvent.VK_END;
    else if (s.equals("pageup")) return KeyEvent.VK_PAGE_UP;
    else if (s.equals("pagedown")) return KeyEvent.VK_PAGE_DOWN;
    else if (s.equals("up")) return KeyEvent.VK_UP;
    else if (s.equals("down")) return KeyEvent.VK_DOWN;
    else if (s.equals("left")) return KeyEvent.VK_LEFT;
    else if (s.equals("right")) return KeyEvent.VK_RIGHT;
    else if (s.equals("clear")) return KeyEvent.VK_CLEAR;
    else if (s.equals("enter")) return KeyEvent.VK_ENTER;
    else if (s.equals("click1") || s.equals("click")) return -MouseEvent.BUTTON1;
    else if (s.equals("click2")) return -MouseEvent.BUTTON2;
    else if (s.equals("click3")) return -MouseEvent.BUTTON3;
    else if (s.length() == 1)
      if (numpad) {
        return KeyEvent.VK_NUMPAD0 + Integer.parseInt(s);
      } else {
        // all other keys
        return KeyEvent.getExtendedKeyCodeForChar(s.charAt(0));
      }
    else return KeyEvent.VK_UNDEFINED;
  }

  public static int getModifiersFromString(String s) {
    int modifiers = 0;

    s = "-" + s.toLowerCase().trim() + "-";
    if (s.contains("-command-")) modifiers |= KeyEvent.META_DOWN_MASK;

    if (s.contains("-control-")) modifiers |= KeyEvent.CTRL_DOWN_MASK;

    if (s.contains("-option-")) modifiers |= KeyEvent.ALT_DOWN_MASK;

    if (s.contains("-shift-")) modifiers |= KeyEvent.SHIFT_DOWN_MASK;
    return modifiers;
  }

  @Override
  public void enterAlt_setglobal(MacroParser.Alt_setglobalContext ctx) {
    addTreeNode(
        ctx, new SetMacroNode(ctx.IDENTIFIER().getText().trim(), true, ctx.set_expression()));
  }

  @Override
  public void enterAlt_set(MacroParser.Alt_setContext ctx) {
    addTreeNode(
        ctx, new SetMacroNode(ctx.IDENTIFIER().getText().trim(), false, ctx.set_expression()));
  }

  @Override
  public void enterStatements(MacroParser.StatementsContext ctx) {
    SequenceMacroNode sequenceMacroNode = new SequenceMacroNode("seq");
    addTreeNode(ctx, sequenceMacroNode);
  }

  @Override
  public void exitStatements(MacroParser.StatementsContext ctx) {
    SequenceMacroNode sequenceMacroNode = (SequenceMacroNode) getMacroNode(ctx);
    ctx.statement().forEach(s -> sequenceMacroNode.addChildMacroNode(getMacroNode(s)));
  }

  @Override
  public void enterStatement_block(MacroParser.Statement_blockContext ctx) {
    SequenceMacroNode sequenceMacroNode = new SequenceMacroNode("seq");
    addTreeNode(ctx, sequenceMacroNode);
  }

  @Override
  public void exitStatement_block(MacroParser.Statement_blockContext ctx) {
    SequenceMacroNode sequenceMacroNode = (SequenceMacroNode) getMacroNode(ctx);
    sequenceMacroNode.addChildMacroNode(getMacroNode(ctx.statements()));
  }

  @Override
  public void enterAlt_statement_block(MacroParser.Alt_statement_blockContext ctx) {
    SequenceMacroNode sequenceMacroNode = new SequenceMacroNode("seq");
    addTreeNode(ctx, sequenceMacroNode);
  }

  @Override
  public void exitAlt_statement_block(MacroParser.Alt_statement_blockContext ctx) {
    SequenceMacroNode sequenceMacroNode = (SequenceMacroNode) getMacroNode(ctx);
    sequenceMacroNode.addChildMacroNode(getMacroNode(ctx.statement_block()));
  }

  @Override
  public void enterAlt_label(MacroParser.Alt_labelContext ctx) {
    addTreeNode(ctx, new LabelMacroNode(ctx.IDENTIFIER().getText().trim()));
  }

  @Override
  public void enterAlt_goto(MacroParser.Alt_gotoContext ctx) {
    addTreeNode(ctx, new GotoMacroNode(ctx.IDENTIFIER().getText().trim()));
  }

  @Override
  public void enterAlt_call(MacroParser.Alt_callContext ctx) {
    addTreeNode(ctx, new CallMacroNode(this, ctx.IDENTIFIER().getText().trim()));
  }

  @Override
  public void enterAlt_if(MacroParser.Alt_ifContext ctx) {
    addTreeNode(ctx, new IfMacroNode(ctx.condition()));
  }

  @Override
  public void enterAlt_elseif(MacroParser.Alt_elseifContext ctx) {
    final IfMacroNode macroNode = new IfMacroNode(ctx.condition());
    addTreeNode(ctx, macroNode);
    ((IfMacroNode) getParentMacroNode(ctx)).setIfElse(macroNode);
  }

  @Override
  public void enterAlt_else(MacroParser.Alt_elseContext ctx) {
    final SequenceMacroNode macroNode = new SequenceMacroNode("else");
    addTreeNode(ctx, macroNode);
    ((IfMacroNode) getParentMacroNode(ctx)).setIfElse(macroNode);
  }

  @Override
  public void enterThen_statements(MacroParser.Then_statementsContext ctx) {
    final SequenceMacroNode macroNode = new SequenceMacroNode("then");
    addTreeNode(ctx, macroNode);
    ((IfMacroNode) getParentMacroNode(ctx)).setIfThen(macroNode);
  }

  @Override
  public void exitThen_statements(MacroParser.Then_statementsContext ctx) {
    SequenceMacroNode sequenceMacroNode = (SequenceMacroNode) getMacroNode(ctx);
    if (ctx.statements() != null) {
      sequenceMacroNode.addChildMacroNode(getMacroNode(ctx.statements()));
    }
  }

  @Override
  public void exitElse_statements(MacroParser.Else_statementsContext ctx) {
    SequenceMacroNode sequenceMacroNode = (SequenceMacroNode) getParentMacroNode(ctx);
    if (ctx.statements() != null) {
      sequenceMacroNode.addChildMacroNode(getMacroNode(ctx.statements()));
    }
  }

  @Override
  public void enterAlt_random(MacroParser.Alt_randomContext ctx) {
    addTreeNode(ctx, new RandomStatementMacroNode(ctx.NOREPEAT() != null));
  }

  @Override
  public void exitAlt_random(MacroParser.Alt_randomContext ctx) {
    RandomStatementMacroNode sequenceMacroNode = (RandomStatementMacroNode) getMacroNode(ctx);
    if (ctx.statements() != null) {
      ctx.statements().stream()
          .map(this::getMacroNode)
          .forEach(sequenceMacroNode::addChildMacroNode);
    }
  }

  @Override
  public void enterAlt_message(MacroParser.Alt_messageContext ctx) {
    addTreeNode(ctx, new MessageStatementMacroNode(ctx.value(), callback));
  }

  @Override
  public void enterAlt_text_command(MacroParser.Alt_text_commandContext ctx) {
    addTreeNode(ctx, new TextCommandStatementMacroNode(ctx.text_command().value()));
  }

  @Override
  public void enterAlt_pause_command(MacroParser.Alt_pause_commandContext ctx) {
    addTreeNode(ctx, new PauseMacroNode(ctx.value()));
  }

  @Override
  public void enterAlt_attribute(MacroParser.Alt_attributeContext ctx) {
    switch (ctx.ATTRIBUTE().getText()) {
      case "$ignore_case":
        {
          getParentMacroNode(ctx).getRoot().addAttribute(MacroMacroNode.Attribute.IGNORE_CASE);
          break;
        }
      case "$any_click":
        {
          getParentMacroNode(ctx).getRoot().addAttribute(MacroMacroNode.Attribute.ANY_CLICK);
          break;
        }
      case "$no_override":
        {
          getParentMacroNode(ctx).getRoot().addAttribute(MacroMacroNode.Attribute.NO_OVERRIDE);
          break;
        }
      default:
        throw new BailErrorListener.BailException(
            "Invalid attribute " + ctx.getText(),
            ctx.getStart().getLine(),
            ctx.getStart().getCharPositionInLine(),
            ctx.getStart().getStartIndex());
    }
  }

  private void addTreeNode(RuleContext node, AbstractMacroNode macroNode) {
    AbstractMacroNode parentTreeNode = getParentMacroNode(node);

    macroNode.setParent(parentTreeNode);

    treeMap.put(node, macroNode);
  }

  private AbstractMacroNode getParentMacroNode(RuleContext node) {
    if (node == null) return null;

    AbstractMacroNode treeNode = treeMap.get(node.parent);

    if (treeNode == null) treeNode = getParentMacroNode(node.parent);

    return treeNode;
  }

  private AbstractMacroNode getMacroNode(RuleContext node) {
    if (node == null) return null;

    /*
     * if (treeNode == null) treeNode = getMacroNode(node.parent());
     */

    return treeMap.get(node);
  }

  public MacroMacroNode getMacro(String trigger) {
    return macroMap.get(trigger.trim());
  }

  public Collection<MacroMacroNode> getMacros() {
    return macroMap.values();
  }

  public static MacroCompiler parseMacroFile(File f, MacroCallback callback) {
    MacroCompiler storage = null;

    try {
      // try to parse with our parser
      MacroLexer macroLexer =
          new MacroLexer(
              CharStreams.fromStream(
                  // The parser expects a newline before EOF, so make sure there is one
                  new SequenceInputStream(
                      new FileInputStream(f), new ByteArrayInputStream(new byte[] {10}))));
      macroLexer.addErrorListener(new BailErrorListener());

      MacroParser macroParser = new MacroParser(new CommonTokenStream(macroLexer));
      macroParser.addErrorListener(new BailErrorListener());

      storage = new MacroCompiler(callback);
      ParseTreeWalker.DEFAULT.walk(storage, macroParser.macros());

    } catch (IOException e) {
      LOGGER.debug("Failed parsing of macro file {}", f, e);
      callback.addSystemMessage(
          "\u2022" + "failed parsing of macro file \"" + f + "\": " + e.getLocalizedMessage());
    } catch (BailErrorListener.BailException e) {
      LOGGER.debug("Failed parsing of macro file {}", f, e);
      callback.addSystemMessage(
          "\u2022"
              + "failed parsing of macro file \""
              + f
              + "("
              + e.getLine()
              + ":"
              + e.getCharPositionInLine()
              + ")"
              + "\": "
              + e.getLocalizedMessage());
    }
    return storage;
  }
}
