package net.heberling.clanlord.macro.interpreter.node;

import java.util.List;
import net.heberling.clanlord.macro.parser.MacroParser;

public class TextCommandStatementMacroNode extends AbstractTerminalMacroNode {
  protected final List<MacroParser.ValueContext> params;

  public TextCommandStatementMacroNode(List<MacroParser.ValueContext> params) {
    this.params = params;
  }

  String getText() {
    StringBuilder buffer = new StringBuilder();

    for (MacroParser.ValueContext element : params) {
      buffer.append(getValue(element).asString());
    }
    // replace all \r with new line
    return buffer.toString().replace('\n', ' ').replaceAll("\\\\r", "\n");
  }

  public Result tick() {
    final String text = getText();
    // return the text and wait as much frames as we have new lines
    return new Result(true, (int) text.chars().filter(c -> c == '\n').count(), text, null);
  }

  public String toString() {
    return getText();
  }

  @Override
  public AbstractMacroNode copy(AbstractMacroNode newParent) {
    TextCommandStatementMacroNode textCommandStatementMacroNode =
        new TextCommandStatementMacroNode(params);
    textCommandStatementMacroNode.setParent(newParent);
    return textCommandStatementMacroNode;
  }
}
