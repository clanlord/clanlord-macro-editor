package net.heberling.clanlord.macro.interpreter.variable;

public abstract class Variable {
  private String[] words;

  public abstract String asString();

  public abstract int asNumber();

  public abstract boolean isNumeric();

  public Variable getWordCount() {
    if (words == null) {
      words = asString().split(" ");
    }
    return new LiteralVariable(words.length);
  }

  public Variable getWord(int word) {
    final int wordCount = getWordCount().asNumber();
    if (word >= 0 && word < wordCount) {
      return new LiteralVariable(words[word]);
    } else {
      return EmptyVariable.INSTANCE;
    }
  }

  public Variable getLetterCount() {
    return new LiteralVariable(asString().length());
  }

  public Variable getLetter(int letter) {
    if (letter >= 0 && letter < asString().length()) {
      return new LiteralVariable(String.valueOf(asString().charAt(letter)));
    } else {
      return EmptyVariable.INSTANCE;
    }
  }
}
