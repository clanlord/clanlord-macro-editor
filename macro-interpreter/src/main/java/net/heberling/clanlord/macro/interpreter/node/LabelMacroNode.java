package net.heberling.clanlord.macro.interpreter.node;

public class LabelMacroNode extends AbstractTerminalMacroNode {

  private final String name;

  public LabelMacroNode(String name) {
    this.name = name;
  }

  public Result tick() {
    return new Result(true, 0, null, null);
  }

  public String getName() {
    return name;
  }

  @Override
  public AbstractMacroNode copy(AbstractMacroNode newParent) {
    LabelMacroNode labelMacroNode = new LabelMacroNode(name);
    labelMacroNode.setParent(newParent);
    return labelMacroNode;
  }

  @Override
  public String toString() {
    return "label " + name;
  }
}
