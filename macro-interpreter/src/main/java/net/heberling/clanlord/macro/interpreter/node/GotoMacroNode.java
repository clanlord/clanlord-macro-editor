package net.heberling.clanlord.macro.interpreter.node;

public class GotoMacroNode extends AbstractTerminalMacroNode {
  private final String name;

  public GotoMacroNode(String name) {
    super();
    this.name = name;
  }

  public Result tick() {
    return new Result(true, 0, null, name);
  }

  @Override
  public AbstractMacroNode copy(AbstractMacroNode newParent) {
    GotoMacroNode gotoMacroNode = new GotoMacroNode(name);
    gotoMacroNode.setParent(newParent);
    return gotoMacroNode;
  }

  @Override
  public String toString() {
    return "goto " + name;
  }
}
