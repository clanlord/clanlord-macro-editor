package net.heberling.clanlord.macro.interpreter.node;

import net.heberling.clanlord.macro.interpreter.variable.LiteralVariable;
import net.heberling.clanlord.macro.interpreter.variable.Variable;
import net.heberling.clanlord.macro.interpreter.variable.VariableStorage;
import net.heberling.clanlord.macro.parser.MacroParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SetMacroNode extends AbstractTerminalMacroNode {
  private static final Logger LOGGER = LoggerFactory.getLogger(SetMacroNode.class);

  private final String identifier;

  private final MacroParser.ValueContext value;
  private final boolean global;

  private final MacroParser.OperatorContext operator;

  public SetMacroNode(
      String identifier, boolean global, MacroParser.Set_expressionContext setExpression) {
    this(identifier, global, setExpression.operator(), setExpression.value());
  }

  public SetMacroNode(
      String identifier,
      boolean global,
      MacroParser.OperatorContext operator,
      MacroParser.ValueContext value) {
    this.identifier = identifier;
    this.global = global;
    this.operator = operator;
    this.value = value;
  }

  public Result tick() {
    Variable newValue = getValue(value);

    Variable oldValue = getVariableStorage().get(identifier);

    (global ? VariableStorage.getGlobalStorage() : getVariableStorage())
        .put(identifier, calculateValue(operator, oldValue, newValue));

    return new Result(true, 0, null, null);
  }

  private Variable calculateValue(
      MacroParser.OperatorContext operator, Variable oldValue, Variable newValue) {
    LOGGER.debug(oldValue + " " + operator + " " + newValue);
    if (operator == null) {
      // just set the variable
      return newValue;
    } else if (oldValue.isNumeric()) {
      if (newValue.isNumeric()) {
        switch (operator.getText()) {
          case "+":
            return new LiteralVariable(oldValue.asNumber() + newValue.asNumber());
          case "-":
            return new LiteralVariable(oldValue.asNumber() - newValue.asNumber());
          case "*":
            return new LiteralVariable(oldValue.asNumber() * newValue.asNumber());
          case "/":
            return new LiteralVariable(oldValue.asNumber() / newValue.asNumber());
          case "%":
            return new LiteralVariable(oldValue.asNumber() % newValue.asNumber());
          default:
            throw new IllegalArgumentException("Unknown Operator " + operator.getText());
        }
      } else {
        throw new IllegalArgumentException("Value " + newValue.asString() + " is not a number");
      }
    } else {
      if (!operator.getText().equals("+")) {
        throw new IllegalArgumentException("Can't use " + operator.getText() + " with strings");
      } else {
        return new LiteralVariable(oldValue.asString() + newValue.asString());
      }
    }
  }

  public String toString() {
    return (global ? "setglobal " : "set ")
        + identifier
        + " "
        + (operator != null ? operator.getText().trim() + " " : "")
        + (value != null ? value.getText() : "<null>");
  }

  @Override
  public AbstractMacroNode copy(AbstractMacroNode newParent) {
    SetMacroNode setMacroNode = new SetMacroNode(identifier, global, operator, value);
    setMacroNode.setParent(newParent);
    return setMacroNode;
  }
}
