package net.heberling.clanlord.macro.interpreter.node;

public class SequenceMacroNode extends AbstractSequenceMacroNode {

  private int currentIndex;

  private Result currentNode;

  public SequenceMacroNode(String name) {
    super(name);
    currentIndex = 0;
  }

  public Result tick() {
    if (statements.isEmpty()) {
      // nothing in here
      return new Result(true, 0, null, null);
    } else if (statements.size() == 1) {
      // the easy case
      return statements.get(0).tick();
    } else {
      while (true) {
        if (currentNode == null) {
          currentNode = statements.get(currentIndex).tick();
        }
        boolean finished = currentNode.isFinished();
        int waitFrames = currentNode.getWaitFrames();
        String text = currentNode.getText();
        String label = currentNode.getLabel();

        if (finished) {
          // the last command is finished, the next comman needs to be calculated
          if (label == null) {
            // no label, next command is the next in list
            currentIndex++;
          } else {
            String finalLabel = label;
            LabelMacroNode labelMacroNode =
                statements.stream()
                    .filter(l -> l instanceof LabelMacroNode)
                    .map(LabelMacroNode.class::cast)
                    .filter(l -> l.getName().equals(finalLabel))
                    .findFirst()
                    .orElse(null);
            if (labelMacroNode == null) {
              // not our label, so we need to be reset, for another call
              currentIndex = Integer.MAX_VALUE;
            } else {
              // continue at the label
              currentIndex = statements.indexOf(labelMacroNode);
              // reset all nodes following the label
              for (int i = currentIndex; i < statements.size(); i++) {
                statements.set(i, statements.get(i).copy(this));
              }
              // label is consumed
              label = null;
            }
          }
          if (currentIndex >= statements.size()) {
            // sequence end, prepare for new call
            currentIndex = 0;
            finished = true;
          } else {
            finished = false;
          }
          currentNode = null;
        }

        if (finished || waitFrames > 0 || text != null) {
          currentNode = null;
          return new Result(finished, waitFrames, text, label);
        }

        // otherwise, tick next in line
      }
    }
  }

  @Override
  public AbstractMacroNode copy(AbstractMacroNode newParent) {
    SequenceMacroNode sequenceMacroNode = new SequenceMacroNode(getName());
    sequenceMacroNode.setParent(newParent);
    statements.forEach(s -> sequenceMacroNode.addChildMacroNode(s.copy(sequenceMacroNode)));
    return sequenceMacroNode;
  }
}
