package net.heberling.clanlord.macro.interpreter.node;

import net.heberling.clanlord.macro.interpreter.MacroCompiler;

public class CallMacroNode extends AbstractTerminalMacroNode {

  private final MacroCompiler macroCompiler;
  private final String calledMacro;
  private AbstractMacroNode macroNode;

  public CallMacroNode(MacroCompiler macroCompiler, String calledMacro) {
    super();
    this.macroCompiler = macroCompiler;
    this.calledMacro = calledMacro;
  }

  public Result tick() {
    if (macroNode == null) {
      // if we have never been called, get the macro
      MacroMacroNode macroMacroNode = macroCompiler.getMacro(calledMacro);
      if (macroMacroNode == null) {
        throw new IllegalStateException("Could not find macro " + calledMacro);
      }

      // we inherit the attributes of the called macro
      macroMacroNode.getAttributes().forEach(getRoot()::addAttribute);

      macroNode = macroMacroNode.getStatement().copy(this);
    }

    // call it
    Result result = macroNode.tick();

    if (result.isFinished()) {
      // submacro is finished, if we get called again, we need a new copy
      macroNode = null;
    }

    return result;
  }

  @Override
  public String toString() {
    return "call " + calledMacro;
  }

  @Override
  public AbstractMacroNode copy(AbstractMacroNode newParent) {
    CallMacroNode callMacroNode = new CallMacroNode(macroCompiler, calledMacro);
    callMacroNode.setParent(newParent);
    return callMacroNode;
  }
}
