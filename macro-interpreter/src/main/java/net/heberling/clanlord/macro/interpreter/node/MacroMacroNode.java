package net.heberling.clanlord.macro.interpreter.node;

import java.util.Collections;
import java.util.EnumSet;
import java.util.Set;
import java.util.function.Consumer;
import net.heberling.clanlord.macro.interpreter.variable.VariableStorage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MacroMacroNode extends AbstractMacroNode {
  public enum Attribute {
    IGNORE_CASE,
    ANY_CLICK,
    NO_OVERRIDE
  }

  private static final Logger LOGGER = LoggerFactory.getLogger(MacroMacroNode.class);
  private final String trigger;
  private final VariableStorage variableStorage = new VariableStorage();
  private AbstractMacroNode statement;
  private boolean activated;
  private boolean needsCleanup;

  private int waitFrames;

  private final Set<Attribute> attributes = EnumSet.noneOf(Attribute.class);

  public MacroMacroNode(String trigger) {
    super();
    this.trigger = trigger;
  }

  public String getTrigger() {
    return trigger;
  }

  public Result tick() {
    LOGGER.trace(this + " got ticked.");
    if (isActivated()) {

      // we are still waiting
      if (--waitFrames > 0) {
        return new Result(false, waitFrames, null, null);
      }

      StringBuilder text = null;

      while (true) {
        Result ret = statement.tick();
        this.waitFrames = ret.waitFrames;

        if (ret.getLabel() != null) {
          // if we're here and are still looking for a label, thats not good
          throw new IllegalStateException("Could not find label " + ret.getLabel());
        }

        if (ret.isFinished()) {
          deactivate();
        }

        if (ret.getText() != null) {
          if (text == null) {
            text = new StringBuilder(ret.getText());
          } else {
            text.append(ret.getText());
          }
        }

        if (ret.isFinished() || ret.waitFrames > 0) {
          return new Result(
              ret.isFinished(), ret.waitFrames, text == null ? null : text.toString(), null);
        }
      }
    } else return new Result(true, 0, null, null);
  }

  public AbstractMacroNode getStatement() {
    return statement;
  }

  public void setStatement(AbstractMacroNode statement) {
    this.statement = statement;
  }

  @Override
  public void visit(Consumer<AbstractMacroNode> consumer) {
    consumer.accept(this);
    if (statement != null) {
      statement.visit(consumer);
    }
  }

  public String toString() {
    return trigger;
  }

  public VariableStorage getVariableStorage() {
    return variableStorage;
  }

  public void activate() {
    if (activated) {
      LOGGER.debug(this + " was still active, resetting");
      deactivate();
    }

    if (needsCleanup) {
      // make sure we don't have any old variables lying around
      variableStorage.clear();
      // make sure we have fresh seqeunce counters
      statement = statement.copy(this);
      needsCleanup = false;
      waitFrames = 0;
    }

    LOGGER.debug(this + " got activated");
    activated = true;
  }

  public void deactivate() {
    activated = false;
    needsCleanup = true;
  }

  public boolean isActivated() {
    return activated;
  }

  @Override
  public MacroMacroNode getRoot() {
    return this;
  }

  public void addAttribute(Attribute attribute) {
    attributes.add(attribute);
  }

  public Set<Attribute> getAttributes() {
    return Collections.unmodifiableSet(attributes);
  }

  @Override
  public AbstractMacroNode copy(AbstractMacroNode newParent) {
    throw new UnsupportedOperationException("Can't copy top level macro " + trigger);
  }

  public boolean isEcho() {
    return getVariableStorage().get("@env.echo").asString().equals("true");
  }

  public boolean isDebug() {
    return getVariableStorage().get("@env.debug").asString().equals("true");
  }

  public boolean isKeyInterrupts() {
    return getVariableStorage().get("@env.key_interrupts").asString().equals("true");
  }

  public boolean isClickInterrupts() {
    return getVariableStorage().get("@env.click_interrupts").asString().equals("true");
  }
}
