package net.heberling.clanlord.macro.interpreter.variable;

/** Returns 0 or "" if it is read */
public class EmptyVariable extends Variable {
  public static final EmptyVariable INSTANCE = new EmptyVariable();

  @Override
  public String asString() {
    return "";
  }

  @Override
  public int asNumber() {
    return 0;
  }

  @Override
  public boolean isNumeric() {
    return true;
  }
}
