package net.heberling.clanlord.macro.interpreter.node;

import java.util.List;
import net.heberling.clanlord.macro.interpreter.MacroCallback;
import net.heberling.clanlord.macro.parser.MacroParser;

public class MessageStatementMacroNode extends TextCommandStatementMacroNode {

  private final MacroCallback callback;

  public MessageStatementMacroNode(List<MacroParser.ValueContext> params, MacroCallback callback) {
    super(params);
    this.callback = callback;
  }

  public Result tick() {
    callback.addMessage(super.getText());
    return new Result(true, 0, null, null);
  }

  public String toString() {
    return "MESSAGE: " + super.toString();
  }

  @Override
  public AbstractMacroNode copy(AbstractMacroNode newParent) {
    MessageStatementMacroNode messageStatementMacroNode =
        new MessageStatementMacroNode(params, callback);
    messageStatementMacroNode.setParent(newParent);
    return messageStatementMacroNode;
  }
}
