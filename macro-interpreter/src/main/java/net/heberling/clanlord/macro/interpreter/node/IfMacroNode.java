package net.heberling.clanlord.macro.interpreter.node;

import java.util.function.Consumer;
import net.heberling.clanlord.macro.interpreter.variable.Variable;
import net.heberling.clanlord.macro.parser.MacroParser;

public class IfMacroNode extends AbstractMacroNode {
  private final MacroParser.ConditionContext condition;

  private AbstractMacroNode ifThen;

  private AbstractMacroNode ifElse;

  private Boolean satisfied;

  public IfMacroNode(MacroParser.ConditionContext condition) {
    super();
    this.condition = condition;
  }

  public void setIfThen(AbstractMacroNode ifThen) {
    this.ifThen = ifThen;
  }

  public void setIfElse(AbstractMacroNode ifElse) {
    this.ifElse = ifElse;
  }

  public Result tick() {
    if (satisfied == null) {
      // only check condition once
      satisfied = checkCondition();
    }
    AbstractMacroNode node = satisfied ? ifThen : ifElse;
    if (node != null) {
      return node.tick();
    } else {
      return new Result(true, 0, null, null);
    }
  }

  public boolean checkCondition() {
    Variable leftValue = getValue(condition.left);
    Variable rightValue = getValue(condition.right);

    if (leftValue.isNumeric() && rightValue.isNumeric()) {
      switch (condition.COMPARATOR().getText()) {
        case "==":
          return leftValue.asNumber() == rightValue.asNumber();
        case "!=":
          return leftValue.asNumber() != rightValue.asNumber();
        case "<=":
          return leftValue.asNumber() <= rightValue.asNumber();
        case "<":
          return leftValue.asNumber() < rightValue.asNumber();
        case ">=":
          return leftValue.asNumber() >= rightValue.asNumber();
        case ">":
          return leftValue.asNumber() > rightValue.asNumber();
        default:
          throw new IllegalArgumentException(
              "Invalid comparator: " + condition.COMPARATOR().getText());
      }
    } else {
      switch (condition.COMPARATOR().getText()) {
        case "==":
          return leftValue.asString().equals(rightValue.asString());
        case "!=":
          return !leftValue.asString().equals(rightValue.asString());
        case "<=":
        case "<":
          return leftValue.asString().contains(rightValue.asString());
        case ">=":
        case ">":
          return rightValue.asString().contains(leftValue.asString());
        default:
          throw new IllegalArgumentException(
              "Invalid comparator: " + condition.COMPARATOR().getText());
      }
    }
  }

  @Override
  public void visit(Consumer<AbstractMacroNode> consumer) {
    consumer.accept(this);
    ifThen.visit(consumer);
    if (ifElse != null) {
      ifElse.visit(consumer);
    }
  }

  @Override
  public String toString() {
    return "if " + condition.getText();
  }

  @Override
  public AbstractMacroNode copy(AbstractMacroNode newParent) {
    IfMacroNode ifMacroNode = new IfMacroNode(condition);
    ifMacroNode.setParent(newParent);
    ifMacroNode.setIfThen(ifThen.copy(ifMacroNode));
    if (ifElse != null) {
      ifMacroNode.setIfElse(ifElse.copy(ifMacroNode));
    }
    return ifMacroNode;
  }
}
