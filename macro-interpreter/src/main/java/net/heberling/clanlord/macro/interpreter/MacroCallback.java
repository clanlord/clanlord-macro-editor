package net.heberling.clanlord.macro.interpreter;

import java.io.File;

public interface MacroCallback {
  void addMessage(String message);

  void addSystemMessage(String message);

  File getMacroFile(String fileName);
}
