package net.heberling.clanlord.macro.interpreter;

import org.antlr.v4.runtime.*;

public class BailErrorListener extends BaseErrorListener {
  @Override
  public void syntaxError(
      Recognizer<?, ?> recognizer,
      Object offendingSymbol,
      int line,
      int charPositionInLine,
      String msg,
      RecognitionException e) {
    int startIndex = -1;

    if (offendingSymbol instanceof Token) {
      startIndex = ((Token) offendingSymbol).getStartIndex();
    } else if (e != null) {
      if (e.getOffendingToken() != null) {
        startIndex = e.getOffendingToken().getStartIndex();
      } else if (e instanceof LexerNoViableAltException) {
        startIndex = ((LexerNoViableAltException) e).getStartIndex();
      }
    }

    throw new BailException(msg, line, charPositionInLine, startIndex);
  }

  public static class BailException extends RuntimeException {
    private final int line;
    private final int charPositionInLine;
    private final int startIndex;

    public BailException(String message, int line, int charPositionInLine, int startIndex) {
      super(message);
      this.line = line;
      this.charPositionInLine = charPositionInLine;
      this.startIndex = startIndex;
    }

    public int getLine() {
      return line;
    }

    public int getCharPositionInLine() {
      return charPositionInLine;
    }

    public int getStartIndex() {
      return startIndex;
    }
  }
}
