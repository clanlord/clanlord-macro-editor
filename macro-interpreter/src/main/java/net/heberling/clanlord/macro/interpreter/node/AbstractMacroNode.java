package net.heberling.clanlord.macro.interpreter.node;

import java.util.function.Consumer;
import net.heberling.clanlord.macro.interpreter.variable.LiteralVariable;
import net.heberling.clanlord.macro.interpreter.variable.Variable;
import net.heberling.clanlord.macro.interpreter.variable.VariableStorage;
import net.heberling.clanlord.macro.parser.MacroParser;

public abstract class AbstractMacroNode {
  private AbstractMacroNode parent;

  public void setParent(AbstractMacroNode parent) {
    this.parent = parent;
  }

  public AbstractMacroNode getParent() {
    return parent;
  }

  /**
   * tick this node. The node is responsible to keep its state and send all necessary things to the
   * server
   *
   * @return the next node that needs processing (null if finished)
   */
  public abstract Result tick();

  public abstract void visit(Consumer<AbstractMacroNode> consumer);

  public VariableStorage getVariableStorage() {
    return getRoot().getVariableStorage();
  }

  public MacroMacroNode getRoot() {
    return getParent().getRoot();
  }

  public abstract AbstractMacroNode copy(AbstractMacroNode newParent);

  @Override
  public abstract String toString();

  protected Variable getValue(MacroParser.ValueContext ctx) {
    if (ctx.NUMBER() != null) {
      int number = Integer.parseInt(ctx.NUMBER().getText());
      if (ctx.MINUS() != null) {
        number = -number;
      }
      return new LiteralVariable(number);
    } else if (ctx.QUOTED() != null) {
      return new LiteralVariable(
          ctx.QUOTED()
              .getText()
              .substring(1, ctx.QUOTED().getText().length() - 1)
              .replace("\\\"", "\""));
    } else {
      return getVariableStorage().get(ctx.IDENTIFIER().getText());
    }
  }

  public static final class Result {
    final boolean finished;
    final int waitFrames;
    final String text;
    final String label;

    public Result(boolean finished, int waitFrames, String text, String label) {
      this.finished = finished;
      this.waitFrames = waitFrames;
      this.text = text;
      this.label = label;
    }

    public boolean isFinished() {
      return finished;
    }

    public int getWaitFrames() {
      return waitFrames;
    }

    public String getText() {
      return text;
    }

    public String getLabel() {
      return label;
    }
  }
}
