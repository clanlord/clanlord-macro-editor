package net.heberling.clanlord.macro.parser;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

public class MacroKeys {

  public static int getKeyCodeFromString(String s) {
    // check for numpad
    boolean numpad = s.contains("numpad");

    // remove modifiers
    if (s.contains("-")) s = s.substring(s.lastIndexOf("-") + 1);

    s = s.trim().toLowerCase();

    if (s.equals("f1")) return KeyEvent.VK_F1;
    else if (s.equals("f2")) return KeyEvent.VK_F2;
    else if (s.equals("f3")) return KeyEvent.VK_F3;
    else if (s.equals("f4")) return KeyEvent.VK_F4;
    else if (s.equals("f5")) return KeyEvent.VK_F5;
    else if (s.equals("f6")) return KeyEvent.VK_F6;
    else if (s.equals("f7")) return KeyEvent.VK_F7;
    else if (s.equals("f8")) return KeyEvent.VK_F8;
    else if (s.equals("f9")) return KeyEvent.VK_F9;
    else if (s.equals("f10")) return KeyEvent.VK_F10;
    else if (s.equals("f11")) return KeyEvent.VK_F11;
    else if (s.equals("f12")) return KeyEvent.VK_F12;
    else if (s.equals("f13")) return KeyEvent.VK_F13;
    else if (s.equals("f14")) return KeyEvent.VK_F14;
    else if (s.equals("f15")) return KeyEvent.VK_F15;
    else if (s.equals("escape")) return KeyEvent.VK_ESCAPE;
    else if (s.equals("minus")) return KeyEvent.VK_MINUS;
    else if (s.equals("delete")) return KeyEvent.VK_DELETE;
    else if (s.equals("tab")) return KeyEvent.VK_TAB;
    else if (s.equals("return")) return KeyEvent.VK_ENTER;
    else if (s.equals("space")) return KeyEvent.VK_SPACE;
    else if (s.equals("help")) return KeyEvent.VK_HELP;
    else if (s.equals("home")) return KeyEvent.VK_HOME;
    else if (s.equals("undo")) return KeyEvent.VK_UNDO;
    else if (s.equals("del")) return KeyEvent.VK_DELETE;
    else if (s.equals("end")) return KeyEvent.VK_END;
    else if (s.equals("pageup")) return KeyEvent.VK_PAGE_UP;
    else if (s.equals("pagedown")) return KeyEvent.VK_PAGE_DOWN;
    else if (s.equals("up")) return KeyEvent.VK_UP;
    else if (s.equals("down")) return KeyEvent.VK_DOWN;
    else if (s.equals("left")) return KeyEvent.VK_LEFT;
    else if (s.equals("right")) return KeyEvent.VK_RIGHT;
    else if (s.equals("clear")) return KeyEvent.VK_CLEAR;
    else if (s.equals("enter")) return KeyEvent.VK_ENTER;
    else if (s.equals("click1") || s.equals("click")) return -MouseEvent.BUTTON1;
    else if (s.equals("click2")) return -MouseEvent.BUTTON2;
    else if (s.equals("click3")) return -MouseEvent.BUTTON3;
    else if (s.length() == 1)
      if (numpad) {
        return KeyEvent.VK_NUMPAD0 + Integer.parseInt(s);
      } else {
        // all other keys
        return KeyEvent.getExtendedKeyCodeForChar(s.charAt(0));
      }
    else return KeyEvent.VK_UNDEFINED;
  }

  public static int getModifiersFromString(String s) {
    int modifiers = 0;

    s = "-" + s.toLowerCase().trim() + "-";
    if (s.contains("-command-")) modifiers |= KeyEvent.META_DOWN_MASK;

    if (s.contains("-control-")) modifiers |= KeyEvent.CTRL_DOWN_MASK;

    if (s.contains("-option-")) modifiers |= KeyEvent.ALT_DOWN_MASK;

    if (s.contains("-shift-")) modifiers |= KeyEvent.SHIFT_DOWN_MASK;
    return modifiers;
  }
}
