/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Grammar for the Clan Lord Macro Language                        *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
grammar Macro;
//Lexer Fragments

@members {
    public boolean isKey(String key){
	    return net.heberling.clanlord.macro.parser.MacroKeys.getKeyCodeFromString(key)!=java.awt.event.KeyEvent.VK_UNDEFINED;
	}

	public boolean isModifier(String key){
        return net.heberling.clanlord.macro.parser.MacroKeys.getModifiersFromString(key)!=0;
	}
}

fragment LETTER
   : [A-Z]
   | [a-z]
   ;

fragment DIGIT
   : [0-9]
   ;

fragment CR
   : '\r'
   ;

fragment LF
   : '\n'
   ;

INCLUDE
   : 'include'
   ;

IF
   : 'if'
   ;

ELSE
   : 'else'
   ;

ELSE_IF
   : 'else' BLANK+ 'if'
   ;

ENDIF
   : 'end' BLANK+ 'if'
   ;

SET_GLOBAL
   : 'setglobal'
   ;

SET
   : 'set'
   ;

RANDOM
   : 'random'
   ;

NOREPEAT
   : 'no-repeat'
   ;

OR
   : 'or'
   ;

END_RANDOM
   : 'end' BLANK+ 'random'
   ;

LABEL
   : 'label'
   ;

GOTO
   : 'goto'
   ;

CALL
   : 'call'
   ;

PAUSE
   : 'pause'
   ;

MESSAGE
   : 'message'
   ;

ATTRIBUTE
   : '$ignore_case'
   | '$any_click'
   | '$no_override'
   ;

COMPARATOR
   : '<='
   | '<'
   | '>='
   | '>'
   | '=='
   | '!='
   ;

PLUS : '+';
MINUS : '-';
MULT : '*';
DIV : '/' {_input.LA(1) != '*' && _input.LA(1) != '/'}?; // make sure we don't match comment start
MOD : '%';

L_BRACE
   : '{'
   ;

R_BRACE
   : '}'
   ;

NUMBER
   : DIGIT+
   ;

SINGLE_QUOTED
   : '\'' (~ ('\n' | '\r' | '\''))*? '\''
   ;

QUOTED
   : '"' ('\\"' | ~ ('\n' | '\r' | '"'))*? '"'
   ;

NEW_LINE
   : CR | LF
   ;

IDENTIFIER
   : '@'? LETTER (LETTER | DIGIT | '_' | '.' | '[' | ']')*
   ;
   //Ignored Tokens
   
BLANK
   : (' ' | '\t' | '\f') -> channel(HIDDEN)
   ;

ML_COMMENT
   : '/*' (ML_COMMENT | .)*? '*/' -> channel(HIDDEN)
   ;

LINE_COMMENT
   : '//' ~ ('\n' | '\r')* -> channel(HIDDEN)
   ;
   //Productions


macros
   : macro* EOF
   ;

macro
   : INCLUDE QUOTED NEW_LINE+ #include_macro
   | set_or_set_global IDENTIFIER set_expression NEW_LINE+ #set_macro
   | SINGLE_QUOTED statement #replacement_macro
   | trigger=QUOTED statement #expression_macro
   | {!(isKey($start.getText())||isModifier($start.getText()))}? IDENTIFIER statement #identifier_macro
   | (modifier+=IDENTIFIER {isModifier($IDENTIFIER.text)}? MINUS)* key=~(NEW_LINE | MINUS) {isKey($key.text)}? statement #key_macro
   | NEW_LINE+ #newline
   ;

statement_block
   : NEW_LINE* L_BRACE NEW_LINE+ statements R_BRACE
   ;

statements
   : statement*
   ;

statement
   : IF condition NEW_LINE+ then_statements optional_else ENDIF NEW_LINE+ # alt_if
   | SET IDENTIFIER set_expression NEW_LINE+ # alt_set
   | SET_GLOBAL IDENTIFIER set_expression NEW_LINE+ # alt_setglobal
   | RANDOM NOREPEAT? NEW_LINE+ statements (OR NEW_LINE+ statements)* END_RANDOM NEW_LINE+ # alt_random
   | LABEL IDENTIFIER NEW_LINE+ # alt_label
   | GOTO IDENTIFIER NEW_LINE+ # alt_goto
   | CALL IDENTIFIER NEW_LINE+ # alt_call
   | PAUSE value NEW_LINE+ # alt_pause_command // may not be QUOTED, to be checked by consumer
   | MESSAGE value+ NEW_LINE+ # alt_message
   | ATTRIBUTE NEW_LINE+ # alt_attribute
   | statement_block NEW_LINE+# alt_statement_block
   | text_command NEW_LINE+# alt_text_command
   ;

optional_else
   : ELSE_IF condition NEW_LINE+ then_statements optional_else # alt_elseif
   | ELSE NEW_LINE* else_statements # alt_else
   | # alt_empty
   ;

then_statements
   : statements
   ;

else_statements
   : statements
   ;

set_or_set_global
   : SET
   | SET_GLOBAL
   ;

text_command
   : value+
   ;

condition
   : left = value COMPARATOR right = value
   ;

set_expression
   :  operator? value
   ;

operator : PLUS|MINUS|MULT|DIV|MOD;

value
   : (PLUS|MINUS)? NUMBER
   | QUOTED
   | IDENTIFIER
   ;

