package net.heberling.clanlord.macro.editor;

import com.ricardojlrufino.jexplorer.JExplorerPanel;
import java.awt.BorderLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import javax.swing.Box;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import org.fife.ui.rsyntaxtextarea.AbstractTokenMakerFactory;
import org.fife.ui.rsyntaxtextarea.RSyntaxDocument;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.Token;
import org.fife.ui.rsyntaxtextarea.TokenMakerFactory;
import org.fife.ui.rsyntaxtextarea.parser.ParserNotice;
import org.fife.ui.rtextarea.RTextScrollPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MacroEditor extends JFrame {
  private static final Logger LOGGER = LoggerFactory.getLogger(MacroEditor.class);
  public static final String TITLE = "Clan Lord Macro Editor";
  public static final Charset UTF8 = StandardCharsets.UTF_8;

  public static final Charset MACROMAN = Charset.forName("MacRoman");

  private boolean dirty = false;
  private LineEnding lineEnding = LineEnding.UNIX;
  private Charset charset = StandardCharsets.UTF_8;
  private File currentFile;
  private final RSyntaxTextArea textArea = new RSyntaxTextArea(40, 120);

  private final DefaultListModel<ParserNotice> noticeListModel = new DefaultListModel<>();

  enum LineEnding {
    WINDOWS("\r\n"),
    MAC("\r"),
    UNIX("\n");

    private final String lineEnding;

    LineEnding(String lineEnding) {

      this.lineEnding = lineEnding;
    }

    public String getLineEnding() {
      return lineEnding;
    }
  }

  static {
    // register our token maker
    AbstractTokenMakerFactory atmf =
        (AbstractTokenMakerFactory) TokenMakerFactory.getDefaultInstance();
    atmf.putMapping(
        "text/clmacro",
        MacroAntlrTokenMaker.class.getName(),
        MacroAntlrTokenMaker.class.getClassLoader());
  }

  public MacroEditor(File macroDirectory) {
    JPanel cp = new JPanel(new BorderLayout());

    JLabel charsetLabel = new JLabel();
    JLabel lineEndingLabel = new JLabel();

    JButton saveButton = new JButton("Save");
    saveButton.addActionListener(e -> save());
    Box toolbar = Box.createHorizontalBox();
    toolbar.add(Box.createHorizontalGlue());
    toolbar.add(lineEndingLabel);
    toolbar.add(Box.createHorizontalStrut(3));
    toolbar.add(charsetLabel);
    toolbar.add(Box.createHorizontalStrut(5));
    toolbar.add(saveButton);
    cp.add(toolbar, BorderLayout.NORTH);

    textArea.setDocument(
        new RSyntaxDocument("/*Open a file on the left to start editing*/") {
          @Override
          public boolean getShouldIndentNextLine(int line) {
            // we can't just override MacroAntlrTokenMaker#getShouldIndentNextLineAfter
            // indentation does not depend only on the last token
            boolean indent = false;
            Token t = getTokenListForLine(line);
            do {
              if (t.getLexeme() != null) {
                switch (t.getLexeme()) {
                  case "if":
                  case "else":
                  case "else if":
                  case "random":
                  case "or":
                  case "{":
                    indent = true;
                    break;
                }
              }
            } while ((t = t.getNextToken()) != null);
            return indent;
          }
        });
    // enabled only if a file is loaded
    textArea.setEnabled(false);
    textArea.setSyntaxEditingStyle("text/clmacro");
    textArea.addParser(new MacroAntlrParser());
    textArea.addPropertyChangeListener(
        RSyntaxTextArea.PARSER_NOTICES_PROPERTY,
        evt -> {
          noticeListModel.clear();
          textArea.getParserNotices().forEach(noticeListModel::addElement);
        });
    textArea
        .getDocument()
        .addDocumentListener(
            new DocumentListener() {
              @Override
              public void insertUpdate(DocumentEvent e) {
                dirty = true;
              }

              @Override
              public void removeUpdate(DocumentEvent e) {
                dirty = true;
              }

              @Override
              public void changedUpdate(DocumentEvent e) {
                dirty = true;
              }
            });
    textArea.setBracketMatchingEnabled(true);
    textArea.setCodeFoldingEnabled(true);
    textArea.setAutoIndentEnabled(true);
    RTextScrollPane sp = new RTextScrollPane(textArea);
    cp.add(sp, BorderLayout.CENTER);

    JExplorerPanel fileExplorerPanel =
        new JExplorerPanel(macroDirectory) {

          @Override
          protected void handleOpenFile(File file) {
            confirmDirtySave(
                "Save changes before closing file?",
                () -> {
                  try {
                    byte[] bytes = Files.readAllBytes(file.toPath());
                    String text = null;
                    try {
                      text = UTF8.newDecoder().decode(ByteBuffer.wrap(bytes)).toString();
                      charset = UTF8;
                    } catch (CharacterCodingException e) {
                      LOGGER.trace("String unparseable for UTF-8");
                    }

                    if (text != null) {
                      // it parsed, but maybe it is macroman, really?
                      // characters "·•–é°®’èö—" taken from
                      // https://stackoverflow.com/a/4200765/755330
                      if (text.contains("\u008E")
                          || text.contains("\u008F")
                          || text.contains("\u009A")
                          || text.contains("\u00A1")
                          || text.contains("\u00A5")
                          || text.contains("\u00A8")
                          || text.contains("\u00D0")
                          || text.contains("\u00D1")
                          || text.contains("\u00D5")
                          || text.contains("\u00E1")) {
                        text = null;
                      }
                    }

                    if (text == null) {
                      // Not UTF8, only other supported is Macroman
                      text = MACROMAN.newDecoder().decode(ByteBuffer.wrap(bytes)).toString();
                      charset = MACROMAN;
                    }

                    // find out which line endings were used
                    if (text.contains(LineEnding.WINDOWS.getLineEnding())) {
                      lineEnding = LineEnding.WINDOWS;
                    } else if (text.contains(LineEnding.MAC.getLineEnding())) {
                      lineEnding = LineEnding.MAC;
                    } else {
                      lineEnding = LineEnding.UNIX;
                    }
                    // normalize all to unix
                    text =
                        text.replace(
                            LineEnding.WINDOWS.getLineEnding(), LineEnding.UNIX.getLineEnding());
                    text =
                        text.replace(
                            LineEnding.MAC.getLineEnding(), LineEnding.UNIX.getLineEnding());
                    textArea.setText(text);
                    textArea.setCaretPosition(0);
                    textArea.setEnabled(true);
                    setTitle(TITLE + ": " + file.getName());
                    lineEndingLabel.setText(lineEnding.name());
                    charsetLabel.setText(charset.displayName());
                    currentFile = file;
                    dirty = false;
                  } catch (IOException e) {
                    showException("Could not load file " + file, e);
                  }
                });
          }

          @Override
          protected void log(String message) {
            LOGGER.trace(message);
          }

          @Override
          public void showException(String message, Exception e) {
            MacroEditor.this.showException(message, e);
          }
        };
    // hide hidden files
    fileExplorerPanel.setFileFilter(pathname -> !pathname.isHidden());
    cp.add(fileExplorerPanel, BorderLayout.WEST);

    JList<ParserNotice> noticeJList = new JList<>(noticeListModel);
    noticeJList.addListSelectionListener(
        e -> {
          ParserNotice selectedValue = noticeJList.getSelectedValue();
          if (selectedValue != null) {
            int offset = selectedValue.getOffset();
            if (offset == -1) {
              // use the line offset
              offset =
                  textArea
                      .getDocument()
                      .getDefaultRootElement()
                      .getElement(selectedValue.getLine())
                      .getStartOffset();
            }
            textArea.setCaretPosition(offset);
          }
        });
    cp.add(new JScrollPane(noticeJList), BorderLayout.SOUTH);

    setContentPane(cp);
    setTitle(TITLE);
    setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
    addWindowListener(
        new WindowAdapter() {
          @Override
          public void windowClosing(WindowEvent e) {
            confirmDirtySave("Save changes before closing editor?", MacroEditor.this::dispose);
          }
        });
    pack();
    setLocationRelativeTo(null);
  }

  private void confirmDirtySave(String message, Runnable action) {
    if (currentFile != null && dirty) {
      int option = JOptionPane.showConfirmDialog(this, message);
      if (option == JOptionPane.CANCEL_OPTION) {
        // keep file open
        return;
      } else if (option == JOptionPane.YES_OPTION) {
        if (!save()) {
          // save didn't work, keep file open
          return;
        }
      }
    }
    action.run();
  }

  private void showException(String message, Exception e) {
    LOGGER.error(message, e);
    JOptionPane.showMessageDialog(this, message);
  }

  private boolean save() {
    try {
      String text = textArea.getText();
      // reset line endings to saved value
      text = text.replace(LineEnding.UNIX.getLineEnding(), lineEnding.getLineEnding());
      Files.write(currentFile.toPath(), text.getBytes(charset));
      dirty = false;
      return true;
    } catch (IOException e) {
      showException("Can't save file", e);
      return false;
    }
  }

  public static void main(String[] args) {
    // Start all Swing applications on the EDT.
    SwingUtilities.invokeLater(
        () -> {
          File file = new File(".");
          if (args != null && args.length == 1) {
            file = new File(args[0]);
          }
          final MacroEditor macroEditor = new MacroEditor(file.getAbsoluteFile());
          macroEditor.setVisible(true);
        });
  }
}
