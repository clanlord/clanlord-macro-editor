package net.heberling.clanlord.macro.editor;

import de.tisoft.rsyntaxtextarea.modes.antlr.AntlrTokenMaker;
import de.tisoft.rsyntaxtextarea.modes.antlr.MultiLineTokenInfo;
import net.heberling.clanlord.macro.parser.MacroLexer;
import org.antlr.v4.runtime.CharStreams;
import org.fife.ui.rsyntaxtextarea.Token;

public final class MacroAntlrTokenMaker extends AntlrTokenMaker {

  public MacroAntlrTokenMaker() {
    super(new MultiLineTokenInfo(0, Token.COMMENT_MULTILINE, "/*", "*/"));
  }

  @Override
  protected int convertType(int type) {
    switch (type) {
      case MacroLexer.ML_COMMENT:
        return Token.COMMENT_MULTILINE;
      case MacroLexer.LINE_COMMENT:
        return Token.COMMENT_EOL;
        // keywords
      case MacroLexer.INCLUDE:

      case MacroLexer.IF:
      case MacroLexer.ELSE_IF:
      case MacroLexer.ELSE:
      case MacroLexer.ENDIF:

      case MacroLexer.RANDOM:
      case MacroLexer.OR:
      case MacroLexer.END_RANDOM:

      case MacroLexer.MESSAGE:

      case MacroLexer.CALL:

      case MacroLexer.LABEL:
      case MacroLexer.GOTO:

      case MacroLexer.SET:
      case MacroLexer.SET_GLOBAL:

      case MacroLexer.PAUSE:
        return Token.RESERVED_WORD;

      case MacroLexer.QUOTED:
        return Token.LITERAL_STRING_DOUBLE_QUOTE;

      case MacroLexer.SINGLE_QUOTED:
        return Token.LITERAL_CHAR;

      case MacroLexer.NUMBER:
        return Token.LITERAL_NUMBER_DECIMAL_INT;

      case MacroLexer.IDENTIFIER:
        return Token.IDENTIFIER;

      case MacroLexer.ATTRIBUTE:
        return Token.VARIABLE;

      case MacroLexer.PLUS:
      case MacroLexer.MINUS:
      case MacroLexer.MULT:
      case MacroLexer.DIV:
      case MacroLexer.MOD:
      case MacroLexer.COMPARATOR:
        return Token.OPERATOR;

      case MacroLexer.L_BRACE:
      case MacroLexer.R_BRACE:
        return Token.SEPARATOR;

      default:
        return Token.ANNOTATION;
    }
  }

  @Override
  public boolean getCurlyBracesDenoteCodeBlocks(int languageIndex) {
    return true;
  }

  @Override
  public String[] getLineCommentStartAndEnd(int languageIndex) {
    return new String[] {"//", null};
  }

  @Override
  protected MacroLexer createLexer(String text) {
    return new MacroLexer(CharStreams.fromString(text));
  }
}
