package net.heberling.clanlord.macro.editor;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JToolBar;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;
import net.heberling.clanlord.macro.interpreter.BailErrorListener;
import net.heberling.clanlord.macro.interpreter.MacroCallback;
import net.heberling.clanlord.macro.interpreter.MacroCompiler;
import net.heberling.clanlord.macro.interpreter.node.AbstractMacroNode;
import net.heberling.clanlord.macro.parser.MacroLexer;
import net.heberling.clanlord.macro.parser.MacroParser;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

public class MacroTreeBuilder extends JFrame {

  private JScrollPane jScrollPane = null;

  private JScrollPane jScrollPane1 = null;

  private JTree structureTree = null;

  private JButton parseButton = null;

  private JEditorPane initStringEditorPane = null;

  private JSplitPane jSplitPane = null;

  private JToolBar jToolBar = null;

  private JPanel jPanel = null; // @jve:decl-index=0:visual-constraint="78,128"

  private JPanel jContentPane = null;

  private JLabel positionLabel = null;

  private JLabel statusLabel = null;

  /** This is the default constructor */
  public MacroTreeBuilder() {
    super();
    initialize();
  }

  private void initialize() {
    this.setContentPane(getJContentPane());
    this.setTitle("Macro Tree Generator (c) AtoroGM");
    this.addWindowListener(
        new java.awt.event.WindowAdapter() {
          public void windowClosing(java.awt.event.WindowEvent e) {
            System.exit(0);
          }
        });
    this.pack();
  }

  /**
   * This method initializes jScrollPane
   *
   * @return javax.swing.JScrollPane
   */
  private JScrollPane getJScrollPane() {
    if (jScrollPane == null) {
      jScrollPane = new JScrollPane();
      jScrollPane.setViewportView(getInitStringEditorPane());
    }
    return jScrollPane;
  }

  /**
   * This method initializes jScrollPane1
   *
   * @return javax.swing.JScrollPane
   */
  private JScrollPane getJScrollPane1() {
    if (jScrollPane1 == null) {
      jScrollPane1 = new JScrollPane();
      jScrollPane1.setViewportView(getStructureTree());
    }
    return jScrollPane1;
  }

  /**
   * This method initializes jTree
   *
   * @return javax.swing.JTree
   */
  private JTree getStructureTree() {
    if (structureTree == null) {
      structureTree = new JTree((TreeNode) null);
      structureTree.setExpandsSelectedPaths(true);
    }
    return structureTree;
  }

  /**
   * This method initializes jButton
   *
   * @return javax.swing.JButton
   */
  private JButton getParseButton() {
    if (parseButton == null) {
      parseButton = new JButton();
      parseButton.setText("Parse");
      parseButton.setToolTipText("Parse the init string.");
      parseButton.addActionListener(
          e -> {
            initStringEditorPane.setCaretPosition(0);

            MacroLexer macroLexer =
                new MacroLexer(CharStreams.fromString(initStringEditorPane.getText() + "\n"));
            macroLexer.addErrorListener(new BailErrorListener());

            MacroParser macroParser = new MacroParser(new CommonTokenStream(macroLexer));
            macroParser.addErrorListener(new BailErrorListener());

            MacroParser.MacrosContext startNode = null;
            try {
              startNode = macroParser.macros();
              statusLabel.setText("Success");
            } catch (BailErrorListener.BailException e1) {
              initStringEditorPane.setCaretPosition(e1.getStartIndex() - 1);
              statusLabel.setText("Error: " + e1.getMessage());
            }

            if (startNode != null) {
              MacroCompiler analyzer =
                  new MacroCompiler(
                      new MacroCallback() {
                        @Override
                        public void addMessage(String message) {}

                        @Override
                        public void addSystemMessage(String message) {}

                        @Override
                        public File getMacroFile(String fileName) {
                          return null;
                        }
                      });
              ParseTreeWalker.DEFAULT.walk(analyzer, startNode);

              DefaultMutableTreeNode root = new DefaultMutableTreeNode("root");

              Map<AbstractMacroNode, DefaultMutableTreeNode> treeNodeMap = new HashMap<>();

              analyzer
                  .getMacros()
                  .forEach(
                      node ->
                          node.visit(
                              abstractMacroNode -> {
                                DefaultMutableTreeNode parent =
                                    treeNodeMap.get(abstractMacroNode.getParent());
                                if (parent == null) {
                                  parent = root;
                                }
                                final DefaultMutableTreeNode newChild =
                                    new DefaultMutableTreeNode(abstractMacroNode);
                                parent.add(newChild);
                                treeNodeMap.put(abstractMacroNode, newChild);
                              }));

              structureTree.setModel(new DefaultTreeModel(root));

              for (int i = 0; i < structureTree.getRowCount(); i++) structureTree.expandRow(i);
            }

            initStringEditorPane.requestFocus();
          });
    }
    return parseButton;
  }

  /**
   * This method initializes jEditorPane
   *
   * @return javax.swing.JEditorPane
   */
  private JEditorPane getInitStringEditorPane() {
    if (initStringEditorPane == null) {
      initStringEditorPane = new JEditorPane();
      initStringEditorPane.addCaretListener(
          e -> {
            if (e.getDot() < e.getMark())
              positionLabel.setText((e.getDot() + 1) + ":" + (e.getMark() + 1));
            else positionLabel.setText((e.getMark() + 1) + ":" + (e.getDot() + 1));
          });
    }
    return initStringEditorPane;
  }

  /**
   * This method initializes jSplitPane
   *
   * @return javax.swing.JSplitPane
   */
  private JSplitPane getJSplitPane() {
    if (jSplitPane == null) {
      jSplitPane = new JSplitPane();
      jSplitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
      jSplitPane.setTopComponent(getJScrollPane());
      jSplitPane.setBottomComponent(getJScrollPane1());
    }
    return jSplitPane;
  }

  /**
   * This method initializes jToolBar
   *
   * @return javax.swing.JToolBar
   */
  private JToolBar getJToolBar() {
    if (jToolBar == null) {
      jToolBar = new JToolBar();
      jToolBar.add(getParseButton());
    }
    return jToolBar;
  }

  /**
   * This method initializes jPanel
   *
   * @return javax.swing.JPanel
   */
  private JPanel getJPanel() {
    if (jPanel == null) {
      statusLabel = new JLabel();
      GridBagConstraints gridBagConstraints12 = new GridBagConstraints();
      jPanel = new JPanel();
      GridBagConstraints gridBagConstraints11 = new GridBagConstraints();
      GridBagConstraints gridBagConstraints13 = new GridBagConstraints();
      jPanel.setLayout(new GridBagLayout());
      gridBagConstraints11.fill = GridBagConstraints.BOTH;
      gridBagConstraints11.gridwidth = 3;
      gridBagConstraints11.gridx = -1;
      gridBagConstraints11.gridy = -1;
      gridBagConstraints11.weightx = 1.0;
      gridBagConstraints11.weighty = 1.0;
      gridBagConstraints12.gridx = 2;
      gridBagConstraints12.gridy = 1;
      gridBagConstraints12.anchor = GridBagConstraints.EAST;
      gridBagConstraints13.gridx = 1;
      gridBagConstraints13.gridy = 1;
      statusLabel.setText("Macro Tree Generator (c) AtoroGM");
      jPanel.add(getJSplitPane(), gridBagConstraints11);
      jPanel.add(getPositionLabel(), gridBagConstraints12);
      jPanel.add(statusLabel, gridBagConstraints13);
    }
    return jPanel;
  }

  /**
   * This method initializes jPanel1
   *
   * @return javax.swing.JPanel
   */
  private JPanel getJContentPane() {
    if (jContentPane == null) {
      jContentPane = new JPanel();
      jContentPane.setLayout(new BorderLayout());
      jContentPane.add(getJPanel(), BorderLayout.CENTER);
      jContentPane.add(getJToolBar(), BorderLayout.NORTH);
    }
    return jContentPane;
  }

  /**
   * This method initializes jLabel
   *
   * @return javax.swing.JLabel
   */
  private JLabel getPositionLabel() {
    if (positionLabel == null) {
      positionLabel = new JLabel();
      positionLabel.setText("1:1");
    }
    return positionLabel;
  }

  public static void main(String[] args) {
    new MacroTreeBuilder().setVisible(true);
  }
} // @jve:decl-index=0:visual-constraint="10,10"
