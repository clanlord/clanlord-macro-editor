package net.heberling.clanlord.macro.editor;

import de.tisoft.rsyntaxtextarea.parser.antlr.AntlrParserBase;
import net.heberling.clanlord.macro.parser.MacroLexer;
import net.heberling.clanlord.macro.parser.MacroParser;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.TokenStream;

public class MacroAntlrParser extends AntlrParserBase<MacroLexer, MacroParser> {
  @Override
  protected MacroLexer createLexer(CharStream input) {
    return new MacroLexer(input);
  }

  @Override
  protected MacroParser createParser(TokenStream input) {
    return new MacroParser(input);
  }

  @Override
  protected void parse(MacroParser parser) {
    parser.macros();
  }
}
